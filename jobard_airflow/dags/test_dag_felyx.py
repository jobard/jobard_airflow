import datetime
import logging
import re
from pathlib import Path

import pendulum
import yaml
from airflow import DAG
from airflow.exceptions import AirflowException
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.utils.task_group import TaskGroup
from dateutil import parser as date_parser

from operators.jobard import JobardOperator

logging_level = Variable.get('logging_level', default_var='INFO')

doc_md = """
### Felyx processor testing DAG
#### Purpose
This DAG is used for testing a Felyx processing chain on a piece of data. It is composed by the following steps :
- Converting the Insitu NetCDF file collections into Parquet files.
- Collect the EO files of the source datasets.
- Extracting with Felyx processor the matchups between Insitu data and EO files.
- Assembling the MMDB products with Felyx processor from the matchups.
- Reporting into Elasticsearch and Grafana the results of the extraction and assemble processing.  
#### Parameters
The following DAG parameters are listed below : 
- `insitu_converters`: dictionary of the insitu converters containing a key-value pair for each converter.  
   The key is the converter identifier and the value a dictionary with 2 keys :  `converter_bin`: for the converter 
   executable path and `converter_env` for the converter environment variable dictionary.
- `insitu_collections_files`: dictionary of the insitu collections formatted as following :   
    key : collection name, value : dictionary with 2 keys : `converter_name` for the converter identifier in DAG parameters
    and `input_files` for path to the NetCDF files.
- `felyx_collect_eo_bin` : path to the felyx-collect-eo-files command.
- `felyx_collect_eo_env`: dictionary for environment variables of felyx-collect-eo-files command.
- `felyx_extraction_bin`: path to the felyx-extraction command.
- `felyx_assemble_bin`: path to the felyx-assemble command.
- `felyx_report_bin`: path to the felyx-report command.
- `felyx_report_env`: dictionary for environment variables of felyx-report command.
- `felyx_config`: path to the felyx processor configuration file.
- `mmdb_ids`: list of the MMDB ids to produce (the id must be the same as the felyx-processor configuration file).
- `skip_sibling_extraction`: Flag (True/False) for skipping sibling dataset extraction.
- `manifest_dir`: path to the manifest root directory.
- `mmdb_dir`: path to the MMDBs directory.
- `child_product_dir`: path to the child MMDBs directory.
- `start`: time range start (datetime) of the extraction. 
- `end`: time range end (datetime) of the extraction.
- `url`: URL of the Jobard API.
- `split`: jobs splitting option (string, ex: '/10').
- `extraction_memory`: extraction command memory option (string, ex: '1G').
- `extraction_walltime`: extraction command walltime option (string, ex: '00:30:00').
- `assembling_memory`: assemble command memory option (string, ex: '1G').
- `assembling_walltime`: assemble command walltime option (string, ex: '00:30:00').
- `connection`: connection name of the remote cluster (string, ex : localhost).
- `workers`: number of workers executing a command (integer).
- `priority`: priority of the jobs (integer, 1: the higher priority, 9: the lower).
#### Notes
- [More information on Felyx](https://felyx.gitlab-pages.ifremer.fr/felyx_docs/)
- [More information on Jobard](https://jobard.gitlab-pages.ifremer.fr/documentation/)
"""

with DAG(
        dag_id='dag_felyx',
        default_args={
            'start_date': pendulum.datetime(2022, 1, 1, tz='UTC'),
        },
        catchup=False,
        tags=['felyx'],
        description='DAG for testing Felyx processor',
        dagrun_timeout=datetime.timedelta(hours=3),
        params={
            'insitu_collections': {
                'cmems_drifter': {
                    'converter_name': 'cmems',
                    'input_files': '/data/felyx/tests/insitu/cmems/netcdf/*/*/*_drifter_global.nc',
                },
                'cmems_argo': {
                    'converter_name': 'cmems',
                    'input_files': '/data/felyx/tests/insitu/cmems/netcdf/*/*/*_argo_global.nc',
                },
                'cmems_moored': {
                    'converter_name': 'cmems',
                    'input_files': '/data/felyx/tests/insitu/cmems/netcdf/*/*/*_moored_global.nc',
                },
                'trusted': {
                    'converter_name': 'trusted',
                    'input_files': '/data/felyx/tests/insitu/trusted/netcdf/db_*.nc',
                },
            },
            'insitu_converters': {
                'cmems': {
                    'converter_bin': '/home/apps/miniconda/envs/felyx_processor_from_git/bin/cmems_to_parquet',
                    'converter_env': {
                        'PYTHONPATH': '/home/apps/miniconda/envs/felyx_processor_from_git/lib/python3.8/site-packages',
                        'LD_PRELOAD': ''
                    },
                },
                'trusted': {
                    'converter_bin': '/home/apps/miniconda/envs/felyx_processor_from_git/bin/trusted_to_parquet',
                    'converter_env': {
                        'PYTHONPATH': '/home/apps/miniconda/envs/felyx_processor_from_git/lib/python3.8/site-packages',
                    },
                    'converter_extra_params': {
                        'start': '20210915',
                        'end': '20210916'
                    }
                },
            },
            'felyx_collect_eo_bin': '/home/apps/miniconda/envs/felyx_processor_from_git/bin/felyx-collect-eo-files',
            'felyx_collect_eo_env': {
                    "PYTHONPATH": "/home/apps/miniconda/envs/felyx_processor_from_git/lib/python3.8/site-packages"
                },
            'felyx_extraction_bin': '/home/apps/miniconda/envs/felyx_processor_test/bin/felyx-extraction',
            'felyx_assemble_bin': '/home/apps/miniconda/envs/felyx_processor_test/bin/felyx-assemble',
            'felyx_report_bin': '/home/apps/miniconda/envs/felyx_report/bin/felyx-report',
            #'felyx_config': '/data/felyx/tests/s3a_mmdb.yaml',
            'felyx_config': '/data/felyx/tests/s3a__cmems_mmdb.yaml',
            'felyx_report_sys_config': '/data/felyx/tests/config_sys_felyx.yaml',
            #'mmdb_ids': ['S3A_SL__OPE_NRT__cmems_drifter'],
            'mmdb_ids': ['S3A_SL__OPE_NRT__cmems_drifter','S3A_SL__OPE_NRT__cmems_argo','S3A_SL__OPE_NRT__cmems_moored','S3A_SL__OPE_NRT__trusted'],
            'skip_sibling_extraction': False,
            'manifest_dir': f'/data/felyx/manif/',
            'mmdb_dir': f'/data/felyx/mmdb/',
            'child_product_dir': None,
            'start': date_parser.parse('2021-09-15'),
            'end': date_parser.parse('2021-09-16'),
            # Config localhost 'url': 'http://localhost:8000/',
            'url': 'http://host.docker.internal:8000',
            'split': '/1',
            'extraction_memory': '1G',
            'extraction_walltime': '00:30:00',
            'assembling_memory': '1G',
            'assembling_walltime': '00:30:00',
            'connection': 'localhost',
            'workers': 2,
            'priority': 1,
        },
        doc_md=doc_md
) as dag:
    logger = logging.getLogger('airflow')
    logger.setLevel(logging.INFO)

    # TODO CR : Check API and daemon health

    # Read Felyx configuration file
    try:
        with open(dag.params['felyx_config'], 'r') as f:
            felyx_cfg = yaml.load(f, Loader=yaml.FullLoader)
    except IOError as e:
        logger.error(f'Error when reading Felyx configuration file : {e}')
        raise AirflowException(f'Error when reading Felyx configuration file : {e}')

    # Create conversion Insitu data to parquet tasks for each insitu collection to convert
    logger.info(f'Create Insitu data conversion tasks')
    with TaskGroup("Conversion", tooltip="conversion task group") as conversion_tasks:
        for collection_id, collection_settings in dag.params['insitu_collections'].items():
            logger.info(f'Collection : {collection_id}')

            # Find converter settings for this collection
            converter_name = collection_settings['converter_name']
            input_path = collection_settings['input_files']
            if converter_name not in dag.params['insitu_converters']:
                logger.error(f'Cannot find {converter_name} converter in DAG parameters')
                raise AirflowException(f'Cannot find {converter_name} converter in DAG parameters')
            converter_bin = dag.params['insitu_converters'][converter_name]['converter_bin']
            converter_env = dag.params['insitu_converters'][converter_name]['converter_env']

            # Find insitu collection settings in Felyx configuration
            if collection_id not in felyx_cfg['SiteCollections']:
                logger.error(f'Cannot find {collection_id} in Felyx configuration file')
                raise AirflowException(f'Cannot find {collection_id} collection in Felyx configuration file')
            collection_cfg = felyx_cfg['SiteCollections'][collection_id]['parquet_config']
            output_dir = collection_cfg['filepath_pattern']
            duration = collection_cfg['frequency']

            converter_params = {'FILE': input_path, 'OUTPUT_DIR': output_dir, 'DURATION': duration}
            converter_command = f'{converter_bin}' + \
                                " -i '{{ params.FILE }}' -o {{ params.OUTPUT_DIR }} -d {{ params.DURATION }}"

            # Add converter extra params
            converter_extra_params_string = ''
            if 'converter_extra_params' in dag.params['insitu_converters'][converter_name]:
                converter_extra_params = dag.params['insitu_converters'][converter_name]['converter_extra_params']
                converter_extra_params_string = "".join(f' --{param_key} {param_value}'
                                                        for param_key, param_value in converter_extra_params.items())

            # Create conversion task
            conversion_task = BashOperator(
                task_id=f'conversion_{collection_id}',
                bash_command=converter_command + converter_extra_params_string,
                params=converter_params,
                env=converter_env,
                # set append_env to True to be able to use env variables like AIRFLOW_HOME from the Airflow environment
                append_env=True,
                dag=dag
            )

    # Create EO files collect task
    logger.info(f'Create collect EO files task')
    dataset_files_dir = Path(dag.folder) / Path('eo_lists')
    logger.info(f'dataset file dir : {dataset_files_dir}')
    dataset_files_dir.mkdir(parents=True, exist_ok=True)
    collect_eo_task = BashOperator(
        task_id='eo_files_collect',
        bash_command=f'rm -f {dataset_files_dir}/*.list && '
                     f'{dag.params["felyx_collect_eo_bin"]}'
                     " {{ params.CONFIGURATION_FILE }}"
                     " --mmdb-ids {{ params.MMDB_IDS }} "
                     " --start {{ params.START }} --end {{ params.END }}"
                     " --output {{ params.OUTPUT }} {{ params.SKIP_SIBLINGS }}"
        ,
        params={
            'CONFIGURATION_FILE': dag.params['felyx_config'],
            'START': dag.params['start'].isoformat(),
            'END': dag.params['end'].isoformat(),
            'OUTPUT': dataset_files_dir,
            'MMDB_IDS': ' '.join(mmdb_id for mmdb_id in dag.params['mmdb_ids']),
            'SKIP_SIBLINGS': "--skip-siblings" if dag.params['skip_sibling_extraction'] is True else "",
        },
        env=dag.params["felyx_collect_eo_env"],
        # set append_env to True to be able to use env variables like AIRFLOW_HOME from the Airflow environment
        append_env=True,
        dag=dag
    )
    # Create the felyx extraction tasks
    logger.info(f'Create Felyx extraction tasks')
    n_workers = dag.params['workers']
    with TaskGroup("Extraction", tooltip="extraction task group") as extraction_tasks:
        for dataset_file in dataset_files_dir.glob('*.list'):
            dataset_id = re.findall('(.+).list', str(dataset_file.name))[0]
            logger.info(f'dataset : {dataset_id} file list : {dataset_file}')
            with open(dataset_file) as fd:
                eo_files = fd.readlines()
                if len(eo_files) == 0:
                    logging.warning(f'skipping matchup extraction for {dataset_id}')
                    continue
                logger.info(
                    f'Creating extraction task for {dataset_id} with '
                    f'{len(eo_files)} files to process')

                extract_task = JobardOperator(
                    task_id=f'felyx_extraction_{dataset_id}',
                    jobard_command=[
                        dag.params['felyx_extraction_bin'],
                        '-c', dag.params['felyx_config'],
                        '--dataset-id', dataset_id,
                        '--manifest-dir', dag.params['manifest_dir'],
                        '--child-product-dir', dag.params['child_product_dir']
                        if dag.params['child_product_dir'] is not None else '',
                        '--inputs'
                    ],
                    jobard_args=[[file.rstrip()] for file in eo_files],
                    jobard_url=dag.params['url'],
                    jobard_split=dag.params['split'],
                    jobard_connection=dag.params['connection'],
                    jobard_extra=[f'n_workers={n_workers}'],
                    jobard_memory=dag.params['extraction_memory'],
                    jobard_walltime=dag.params['extraction_walltime'],
                    jobard_priority=dag.params['priority'],
                )

    # Create the felyx assembling tasks
    logger.info(f'Create Felyx assembling tasks')
    with TaskGroup("Assembling", tooltip="assembling task group") as assembling_tasks:
        for mmdb_id in dag.params['mmdb_ids']:
            logger.info(f'Creating assembling task for {mmdb_id}')
            assemble_task = JobardOperator(
                task_id=f'felyx_assembling_{mmdb_id}',
                jobard_command=[
                    dag.params['felyx_assemble_bin'],
                    '-c', dag.params['felyx_config'],
                    '-o', dag.params['mmdb_dir'],
                    '--manifest-dir', dag.params['manifest_dir'],
                    '--from-manifests',
                    '--extract-from-source',
                    '--start', dag.params['start'].isoformat(),
                    '--end', dag.params['end'].isoformat(),
                    '--matchup-dataset'
                ],
                jobard_args=[[mmdb_id]],
                jobard_url=dag.params['url'],
                jobard_split=dag.params['split'],
                jobard_connection=dag.params['connection'],
                jobard_extra=[f'n_workers={n_workers}'],
                jobard_memory=dag.params['assembling_memory'],
                jobard_walltime=dag.params['assembling_walltime'],
                jobard_priority=dag.params['priority'],
            )

    # Create Felyx reporting task
    logger.info(f'Create Felyx reporting task')
    reporting_task = BashOperator(
        task_id='felyx_reporting',
        bash_command=f'{dag.params["felyx_report_bin"]}'
                     " -c {{ params.CONFIGURATION_FILE }}"
                     " --start {{ params.START }} --end {{ params.STOP }}"
                     " --manifest-dir {{ params.MANIFEST_DIR }} --mdb-dir {{ params.MDB_DIR }}",
        params={
            'CONFIGURATION_FILE': dag.params['felyx_config'],
            'START': dag.params['start'].isoformat(),
            'STOP': dag.params['end'].isoformat(),
            'MANIFEST_DIR': dag.params['manifest_dir'],
            'MDB_DIR': dag.params['mmdb_dir']
        },
        env={"FELYX_SYS_CFG": '{{ dag.params["felyx_report_sys_config"] }}'},
        dag=dag
    )

    # Specify tasks dependencies
    [conversion_tasks, collect_eo_task] >> extraction_tasks >> assembling_tasks >> reporting_task





